using Microsoft.EntityFrameworkCore;

namespace TodoApi.Models
{
    public class TodoApiDbContext:DbContext
    {
        public TodoApiDbContext(DbContextOptions<TodoApiDbContext>options):base(options)
        {
            
        }
        public DbSet<Employee>Mohamed_Employees{get;set;}
        public DbSet<Department>Mohamed_Departments{get;set;}

        // protected override void OnModelCreating(ModelBuilder modelBuilder)
        // {
        //     modelBuilder.Entity<Employee>()
        //         .HasOne(d => d.Department)
        //         .WithMany(e => e.Mohamed_Employees)
        //         .IsRequired()
        //         .OnDelete(DeleteBehavior.SetNull);
        // }
    }
}